#include <windows.h>
#include <commctrl.h>
#include <stdio.h>

#define snprintf _snprintf

#define CONFIGFILE "./TL2Cam.ini"
#define CLASSNAME   "TL2Cam"

static HMODULE dll = NULL;
static HOOKPROC func = NULL;
static HWND hwin = NULL;
static HHOOK hook = NULL;
static HANDLE proc = NULL;

static HWND btn = NULL;
static int *keyp = NULL;

static void AddHook();
static void RemoveHook();

static char *key_to_str(int key, char *str, int len)
{
	switch (key)
	{
	case VK_LBUTTON:
		return "LMB";

	case VK_RBUTTON:
		return "RMB";

	case VK_MBUTTON:
		return "MMB";
	}

	GetKeyNameText(MapVirtualKey(key, 0) << 16, str, len);
	return str;
}

static LRESULT CALLBACK WindowProc(HWND win, UINT msg, WPARAM wp, LPARAM lp)
{
	static float stepx = 0.2;
	static float stepy = 0.2;
	static int key = VK_MBUTTON; 
	static int rkey = VK_BACK; 
	static HWND w_hs, w_hst;
	static HWND w_vs, w_vst;

	char str[32];
	HINSTANCE hi;
	TEXTMETRIC tm;
	int i, j;


	switch (msg)
	{
	case WM_CREATE:
		GetPrivateProfileString("Settings", "HSensitivity", "0.2", str, sizeof(str), CONFIGFILE);
		stepx = atof(str);
		GetPrivateProfileString("Settings", "VSensitivity", "0.2", str, sizeof(str), CONFIGFILE);
		stepy = atof(str);
		key = GetPrivateProfileInt("Settings", "Key", VK_MBUTTON, CONFIGFILE);
		rkey = GetPrivateProfileInt("Settings", "Reset", VK_BACK, CONFIGFILE);

		hi = GetModuleHandle(NULL);

		i = 2;
		j = 16;
		if (GetTextMetrics(GetDC(win), &tm))
		{
			j = tm.tmHeight;
			i = (20 - j) / 2;
		}

		CreateWindow("STATIC", "Horizonal Sensitivity",
			WS_CHILD | WS_VISIBLE | SS_CENTER,
			0, i, 250, j,
			win, NULL, hi, NULL);

		snprintf(str, sizeof(str), "%.2f", stepx);
		w_hst = CreateWindow("STATIC", str,
			WS_CHILD | WS_VISIBLE | SS_CENTER,
			200, 20 + i, 50, j,
			win, NULL, hi, NULL);

		w_hs = CreateWindow(TRACKBAR_CLASS, "",
			WS_CHILD | WS_VISIBLE | TBS_HORZ,
			0, 20, 200, 20, 
			win, NULL, hi, NULL);

		SendMessage(w_hs, TBM_SETRANGE,  TRUE,  MAKELONG(0, 100));
		SendMessage(w_hs, TBM_SETPAGESIZE, 0,  10); 
		SendMessage(w_hs, TBM_SETPOS, TRUE,  (int)(stepx * 100.0)); 
		SendMessage(w_hs, TBM_SETBUDDY, FALSE, (LPARAM)w_hst);

		CreateWindow("STATIC", "Vertical Sensitivity",
			WS_CHILD | WS_VISIBLE | SS_CENTER,
			0, 40 + i, 250, j,
			win, NULL, hi, NULL);

		snprintf(str, sizeof(str), "%.2f", stepy);
		w_vst = CreateWindow("STATIC", str,
			WS_CHILD | WS_VISIBLE | SS_CENTER,
			200, 60 + i, 50, j, 
			win, NULL, hi, NULL);

		w_vs = CreateWindow(TRACKBAR_CLASS, "",
			WS_CHILD | WS_VISIBLE | TBS_HORZ,
			0, 60, 200, 20, 
			win, NULL, hi, NULL);

		SendMessage(w_vs, TBM_SETRANGE,  TRUE,  MAKELONG(0, 100));
		SendMessage(w_vs, TBM_SETPAGESIZE, 0,  10); 
		SendMessage(w_vs, TBM_SETPOS, TRUE,  (int)(stepy * 100.0)); 
		SendMessage(w_vs, TBM_SETBUDDY, FALSE, (LPARAM)w_vst);

		CreateWindow("STATIC", "Camera Button",
			WS_CHILD | WS_VISIBLE | SS_CENTER,
			20, 90 + i, 100, j,
			win, NULL, hi, NULL);

		
		CreateWindow("BUTTON", key_to_str(key, str, sizeof(str)),
			WS_CHILD | WS_VISIBLE | BS_CHECKBOX | BS_PUSHLIKE,
			130, 90, 100, 20, 
			win, (HMENU)1, hi, NULL);

		CreateWindow("STATIC", "Reset Button",
			WS_CHILD | WS_VISIBLE | SS_CENTER,
			20, 120 + i, 100, j,
			win, NULL, hi, NULL);

		CreateWindow("BUTTON", key_to_str(rkey, str, sizeof(str)),
			WS_CHILD | WS_VISIBLE | BS_CHECKBOX | BS_PUSHLIKE,
			130, 120, 100, 20, 
			win, (HMENU)2, hi, NULL);

		CreateWindowA("BUTTON", "Save",
			WS_CHILD | WS_VISIBLE,
			75, 150, 100, 20, 
			win, (HMENU)3, hi, NULL);

		return 0;

	case WM_DESTROY:
		PostQuitMessage(0);
		return 0;

	case WM_HSCROLL:
		if (lp == (LPARAM)w_hs)
		{
			stepx = (float)(SendMessage(w_hs, TBM_GETPOS, 0, 0)) / 100.0;
			snprintf(str, sizeof(str), "%.2f", stepx);
			SendMessage(w_hst, WM_SETTEXT, 0, (LPARAM)str);
		}
		else if (lp == (LPARAM)w_vs)
		{
			stepy = (float)(SendMessage(w_vs, TBM_GETPOS, 0, 0)) / 100.0;
			snprintf(str, sizeof(str), "%.2f", stepy);
			SendMessage(w_vst, WM_SETTEXT, 0, (LPARAM)str);
		}
		return 0;

	case WM_COMMAND:
		if (btn)
		{
			SendMessage(btn, BM_SETCHECK, BST_UNCHECKED, 0);
			btn = NULL;
		}

		switch (wp)
		{
		case 1:
			SetFocus(win);
			keyp = &key;
			btn = (HWND)lp;
			SendMessage(btn, BM_SETCHECK, BST_CHECKED, 0);
			break;

		case 2:
			SetFocus(win);
			keyp = &rkey;
			btn = (HWND)lp;
			SendMessage(btn, BM_SETCHECK, BST_CHECKED, 0);
			break;

		case 3:
			snprintf(str, sizeof(str), "%.2f", stepx);
			WritePrivateProfileString("Settings", "HSensitivity", str, CONFIGFILE);
			snprintf(str, sizeof(str), "%.2f", stepy);
			WritePrivateProfileString("Settings", "VSensitivity",  str, CONFIGFILE);
			snprintf(str, sizeof(str), "0x%02x", key);
			WritePrivateProfileString("Settings", "Key", str, CONFIGFILE);
			snprintf(str, sizeof(str), "0x%02x", rkey);
			WritePrivateProfileString("Settings", "Reset", str, CONFIGFILE);
			WritePrivateProfileString(NULL, NULL, NULL, CONFIGFILE);

			RemoveHook(TRUE);
			break;
		}

		return 0;

	case WM_KILLFOCUS:
		if (btn)
		{
			SendMessage(btn, BM_SETCHECK, BST_UNCHECKED, 0);
			btn = NULL;
		}
		return 0;
	}
	
	return DefWindowProc(win, msg, wp, lp);
}

static void KeyEvent(int key)
{
	if (btn)
	{
		char str[32];

		*keyp = key;
		SendMessage(btn, WM_SETTEXT, 0, (LPARAM)key_to_str(key, str, sizeof(str)));
		SendMessage(btn, BM_SETCHECK, BST_UNCHECKED, 0);
		btn = NULL;
	}
}

int WINAPI WinMain(HINSTANCE hi, HINSTANCE hpi, LPSTR cmd, int show)
{
	WNDCLASS wc;
	HWND win;
	RECT rect = { 0, 0, 250, 180 };

	dll = LoadLibrary("TL2Cam.dll");
	if (!dll) return 1;

	func = (void *)GetProcAddress(dll, "TL2CamMouseHook");
	if (!func) return 1;

	InitCommonControls();

	if (!AdjustWindowRect(&rect, WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX, FALSE))
		return 1;

	ZeroMemory(&wc, sizeof(wc));
	
	wc.lpfnWndProc = WindowProc;
	wc.hInstance = hi;
	wc.lpszClassName = CLASSNAME;
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH)COLOR_BACKGROUND;

	if (!RegisterClass(&wc))
		return 1;

	win = CreateWindow(CLASSNAME, "Torchlight 2 Camera Mod",
		WS_CAPTION|WS_SYSMENU|WS_MINIMIZEBOX,
		CW_USEDEFAULT, CW_USEDEFAULT,
		rect.right - rect.left, rect.bottom - rect.top,
		NULL, NULL, hi, NULL);
	if (!win) return 1;

	ShowWindow(win, show); 

	for(;;)
	{
		MSG msg;

		if (!hook)
		{
			AddHook();
		}

		while (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			switch (msg.message)
			{
			case WM_KEYDOWN:
				KeyEvent(msg.wParam);
				break;

			case WM_LBUTTONDOWN:
				KeyEvent(VK_LBUTTON);
				break;

			case WM_RBUTTONDOWN:
				KeyEvent(VK_RBUTTON);
				break;

			case WM_MBUTTONDOWN:
				KeyEvent(VK_MBUTTON);
				break;

			case WM_QUIT:
				RemoveHook(FALSE);
				return 0;
			}

			TranslateMessage(&msg);
			DispatchMessage(&msg); 
		};

		if (proc)
		{
			DWORD code;

			MsgWaitForMultipleObjects(1, &proc, FALSE, INFINITE, QS_ALLINPUT);

			if (!GetExitCodeProcess(proc, &code) || code != STILL_ACTIVE)
				RemoveHook(FALSE);
		}
		else
		{
			MsgWaitForMultipleObjects(0, NULL, FALSE, 1000, QS_ALLINPUT);
		}
	}
}

static void AddHook()
{
	HWND win = NULL;
	DWORD pid;
	DWORD thread;

	while ((win = FindWindowEx(0, win, "OgreD3d9Wnd", 0)))
	{
		char str[11];
		GetWindowText(win, str, 11);
		if (!strcmp(str, "Torchlight"))
			goto found;
	}

	while ((win = FindWindowEx(0, win, "OgreGLWindow", 0)))
	{
		char str[11];
		GetWindowText(win, str, 11);
		if (!strcmp(str, "Torchlight"))
			goto found;
	}

	return;

found:
	thread = GetWindowThreadProcessId(win, &pid);
	if (!thread) return;

	proc = OpenProcess(SYNCHRONIZE|PROCESS_QUERY_INFORMATION, FALSE, pid);
	if (!proc) return;

	hook = SetWindowsHookEx(WH_MOUSE, func, dll, thread);
	if (hook)
	{
		hwin = win;
		return;
	}

	CloseHandle(proc);
	proc = 0;
}

static void RemoveHook(BOOL wait)
{
	if (hook)
	{
		UnhookWindowsHookEx(hook);
		hook = NULL;
	}

	if (proc)
	{
		CloseHandle(proc);
		proc = NULL;
	}

	if (hwin)
	{
		if (wait)
		{
			DWORD result;
			SendMessageTimeout(hwin, WM_NULL, 0, 0, SMTO_NORMAL, 1000, &result);
		}
		hwin = NULL;
	}
}
