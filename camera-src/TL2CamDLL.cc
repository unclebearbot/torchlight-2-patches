#include <windows.h>
#include <math.h>
#include <stdio.h>

#define CONFIGFILE "./TL2Cam.ini"

namespace Ogre
{
class Vector3
{
public:
	float x, y, z;
};

class Camera;
}

static Ogre::Camera *cam = NULL;
static Ogre::Vector3 pos;
static float yaw = 135.0;
static float pitch = 45.0;
static float stepx = 0.2;
static float stepy = 0.2;
static int key = VK_MBUTTON; 
static int reset = VK_BACK; 

namespace Ogre
{
class Camera
{
public:
	DECLSPEC_IMPORT void __thiscall lookAt(float, float, float);
	DECLSPEC_IMPORT void __thiscall setPosition(float, float, float);

	void __thiscall myLookAt(const Vector3& vec)
	{
		if (cam == this)
		{
			float x = pos.x - vec.x;
			float y = pos.y - vec.y;
			float z = pos.z - vec.z;

			if (GetKeyState(reset) < 0)
			{
				pitch = atan2(y, sqrt(x * x + z * z)) * 180.0 / M_PI;
				yaw = atan2(x, z) * 180.0 / M_PI;

				setPosition(pos.x, pos.y, pos.z);
			}
			else
			{
				float d = sqrt((x * x) + (y * y) + (z * z));

				y = sin(pitch * M_PI / 180.0) * d;
				z = cos(pitch * M_PI / 180.0) * d;

				x = sin(yaw * M_PI / 180.0) * z;
				z = cos(yaw * M_PI / 180.0) * z;

				setPosition(vec.x + x, vec.y + y, vec.z + z);
			}
		}
		else if (cam == NULL)
		{
			cam = this;
		}

		lookAt(vec.x, vec.y, vec.z);
	}

	void __thiscall mySetPosition(const Vector3& vec)
	{
		if (cam == this)
		{
			pos = vec;
		}
		else
		{
			setPosition(vec.x, vec.y, vec.z);
		}
	}
};
}

static void hook_func(void *ofn, void *nfn, void *save)
{
	HANDLE proc = GetCurrentProcess();

	ReadProcessMemory(proc, ofn, save, 5, 0);

	unsigned char jmp[5];
	unsigned addr = unsigned(nfn) - (unsigned(ofn) + 5);

	jmp[0] = 0xe9;
	jmp[1] = addr;
	jmp[2] = addr >> 8;
	jmp[3] = addr >> 16;
	jmp[4] = addr >> 24;

	WriteProcessMemory(proc, ofn, jmp, 5, 0);
}

extern "C" BOOL WINAPI DllMain(HINSTANCE hinst, DWORD reason, LPVOID ptr)
{
	static void *look_at = NULL;
	static void *set_pos = NULL;
	static char saved_look_at[5];
	static char saved_set_pos[5];
	HMODULE dll;

	switch (reason)
	{
	case DLL_PROCESS_ATTACH:

		if ((dll = GetModuleHandle("OgreMain.dll")))
		{
			if (!look_at && (look_at = (void *)GetProcAddress(dll, "?lookAt@Camera@Ogre@@QAEXABVVector3@2@@Z")))
			{
				hook_func(look_at, (void *)&Ogre::Camera::myLookAt, saved_look_at);
			}

			if (!set_pos && (set_pos = (void *)GetProcAddress(dll, "?setPosition@Camera@Ogre@@QAEXABVVector3@2@@Z")))
			{
				hook_func(set_pos, (void *)&Ogre::Camera::mySetPosition, saved_set_pos);
			}
		}

		char str[32];
		GetPrivateProfileString("Settings", "HSensitivity", "0.2", str, sizeof(str), CONFIGFILE);
		stepx = atof(str);
		GetPrivateProfileString("Settings", "VSensitivity", "0.2", str, sizeof(str), CONFIGFILE);
		stepy = atof(str);
		key = GetPrivateProfileInt("Settings", "Key", VK_MBUTTON, CONFIGFILE);
		reset = GetPrivateProfileInt("Settings", "Reset", VK_BACK, CONFIGFILE);

		break;

	case DLL_PROCESS_DETACH:

		if (look_at)
		{
			WriteProcessMemory(GetCurrentProcess(), look_at, saved_look_at, 5, 0);
			look_at = NULL;
		}

		if (set_pos)
		{
			WriteProcessMemory(GetCurrentProcess(), set_pos, saved_set_pos, 5, 0);
			set_pos = NULL;
		}

		break;
	}

	return TRUE;
}

extern "C" DECLSPEC_EXPORT LRESULT CALLBACK TL2CamMouseHook(int code, WPARAM wp, LPARAM lp)
{
	static BOOL btn = 0;
	static LONG x, y;
	PMOUSEHOOKSTRUCT m = (PMOUSEHOOKSTRUCT)lp;

	if (code == HC_ACTION)
	{
		if (GetKeyState(key) < 0)
		{
			if (btn)
			{
				LONG dx = m->pt.x - x;
				LONG dy = m->pt.y - y;

				if (dx)
				{
					float f = yaw - dx * stepx;
					while (f < -180.0) f += 360.0;
					while (f > 180.0) f -= 360.0;
					yaw = f;
				}

				if (dy)
				{
					float f = pitch + dy * stepy;
					if (f < -89.0) f = -89.0;
					if (f > 89.0) f = 89.0;
					pitch = f;
				}
			}

			btn = 1;
			x = m->pt.x;
			y = m->pt.y;
		}
		else
		{
			btn = 0;
		}
	}

	return CallNextHookEx(0, code, wp, lp);
}
